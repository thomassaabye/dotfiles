:set number
:set relativenumber
:set autoindent
:set tabstop=4 shiftwidth=4 expandtab
:set smarttab
:set softtabstop=4
:set mouse=a


call plug#begin(stdpath("data") . '/plugged')

" rust-tools.nvim
Plug 'neovim/nvim-lspconfig'
Plug 'simrat39/rust-tools.nvim'

Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'nvim-treesitter/nvim-treesitter-refactor'

Plug 'https://github.com/tpope/vim-surround' " Surrounding ysw)
Plug 'https://github.com/tpope/vim-repeat' " Repeat support for Surrounding and others plugin
Plug 'https://github.com/preservim/nerdtree' " NerdTree
Plug 'https://github.com/tpope/vim-commentary' " For Commenting gcc & gc
Plug 'https://github.com/tpope/vim-obsession' " For saving neovim sessions when saving tmux sessions

"Plugin 'scrooloose/nerdcommenter' "todo: decide which commentary plugin to
"use

" Telescope is fzf for neovim
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'

Plug 'https://github.com/vim-airline/vim-airline' " Status bar
Plug 'vim-airline/vim-airline-themes'

" Markdown support
" https://github.com/plasticboy/vim-markdown
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'

" Increament with ctrl-a and decrement ctrl-z, use ctrl-v and :I to do magic
" inserts. See http://www.drchip.org/astronaut/vim/#VISINCR
Plug 'vim-scripts/VisIncr'

Plug 'https://github.com/lifepillar/pgsql.vim' " PSQL Pluging needs :SQLSetType pgsql.vim
Plug 'https://github.com/ap/vim-css-color' " CSS Color Preview
Plug 'https://github.com/rafi/awesome-vim-colorschemes' " Retro Scheme
" Plug 'https://github.com/neoclide/coc.nvim'  " Auto Completion
Plug 'https://github.com/tc50cal/vim-terminal' " Vim Terminal
Plug 'https://github.com/preservim/tagbar' " Tagbar for code navigation
"Plug 'https://github.com/terryma/vim-multiple-cursors' " CTRL + N for multiple cursors

"Plug 'https://github.com/ryanoasis/vim-devicons' " Developer Icons


set encoding=UTF-8
set nofoldenable

call plug#end()


" Auto start NERD tree when opening a directory
" autocmd VimEnter * if argc() == 2 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | wincmd p | endif

" Auto start NERD tree if no files are specified
" autocmd StdinReadPre * let s:std_in=1
" autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | exe 'NERDTree' | endif

" Let quit work as expected if after entering :q the only window left open is NERD Tree itself
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif




:set completeopt-=preview " For No Previews

:colorscheme onedark

" Handling copy to/from wsl on windows: https://waylonwalker.com/vim-wsl-clipboard/
if system('uname -r') =~ "microsoft"
  augroup Yank
  autocmd!
  autocmd TextYankPost * :call system('/mnt/c/windows/system32/clip.exe ',@")
  augroup END
endif
nmap PP :r!powershell.exe -command "Get-Clipboard"<CR>


" Override symbols if they are missing
" let g:NERDTreeDirArrowExpandable="+"
" let g:NERDTreeDirArrowCollapsible="~"


" --- Just Some Notes ---
" :PlugClean :PlugInstall :UpdateRemotePlugins
"
" :CocInstall coc-python
" :CocInstall coc-clangd
" :CocInstall coc-snippets
" :CocCommand snippets.edit... FOR EACH FILE TYPE
"
" Installing nerd-font
" $ cd ~/Downloads
" $ wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/JetBrainsMono.zip

" $ # unpack fonts
" $ mkdir ~/.fonts && cd ~/.fonts
" $ unzip ~/Downloads/JetBrainsMono.zip

" $ # rebuild Fedora font cache
" $ fc-cache

" Jusing buffers instead of tabs


" air-line
let g:airline_powerline_fonts = 1
let g:airline_theme='monochrome'

" Enable the list of buffer
let g:airline#extensions#tabline#enabled = 1
" Show just the filename
let g:airline#extensions#tabline#fnamemod = ':t'

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
 endif

" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'" unicode symbols

inoremap <expr> <Tab> pumvisible() ? coc#_select_confirm() : "<Tab>"

" ============ Key map ==============
" change the leader key from "\" to " " ("," is also popular)
let mapleader=" "

" Find files using Telescope command-line sugar.
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>

nnoremap <C-n> :NERDTreeToggle<CR>
nnoremap <C-l> :call CocActionAsync('jumpDefinition')<CR>

nmap <F8> :TagbarToggle<CR>

" toggle tagbar
nnoremap <silent> <leader>tb :TagbarToggle<CR>
